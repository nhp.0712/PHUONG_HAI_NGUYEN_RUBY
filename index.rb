# Function#1: Write a function that prints the string “Hello,world.”
#-------------------------------Function 1-------------------------------------
puts "\nFunction 1\n----------"
def helloWorld
	puts "Hello, world."
end
helloWorld()

# Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
#    The function should ﬁnd the index of substring in string and print that index.
#-------------------------------Function 2-------------------------------------
puts "\n\nFunction 2\n----------"
def find(string, substring)
	if string[substring]
		puts "Index of 'World' in 'hello World' is #{string.index(substring)}"
	end
end
find("hello World", "World")

# Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
#    the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
#    ODD index. The array passed into this function must contain at least 100 randomly generated values. 
#    Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
#-------------------------------Function 3-------------------------------------
puts "\n\nFunction 3\n----------"
arr = (0..99).collect{rand(0.. 100)}
def SortArray(arr)
	puts "Sorted Order"
	arr.sort!
	p arr
	puts "\n\nReverse Order"
	arr.reverse!
	p arr
	puts "\n\nItems with ODD index"
	puts arr.values_at(* arr.each_index.select {|i| i.odd?}).inspect
end 
SortArray(arr)

# Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
#-------------------------------Function 4-------------------------------------
puts "\n\nFunction 4\n----------"
arr = (0..99).collect{rand(100)}
def SelectionSort(arr)
	for i in 0..arr.length-1
	index = i
		for j in (i+1)..arr.length-1
			if arr[index] > arr[j]
				temp = arr[j]
				arr[j] = arr[index]
				arr[index] = temp
			end
		end
	end
	puts "Selection Sort:"
	p arr
end 
SelectionSort(arr)

# Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
#    The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
#-------------------------------Function 5-------------------------------------
puts "\n\nFunction 5\n----------"
def fiboRecur(num)
	return num if (0..1).include? num
	fiboRecur(num-1) + fiboRecur(num-2)
end
puts "Fibonacci series of 30 is: #{fiboRecur(30)}"

# Function#6: Implement a function to calculate the Fibonacci Series iteratively
#-------------------------------Function 6-------------------------------------
puts "\n\nFunction 6\n----------"
def fiboIter(num)
	a = 0
	b = 1
	i = 1
	while i < num do
		temp = a
		a = b
		b = temp + b
		i = i + 1
	end
	return b
end
puts "Fibonacci series of 10 is: #{fiboIter(10)}"

# Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
#    return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
#    0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
#-------------------------------Function 7-------------------------------------
puts "\n\nFunction 7\n----------"
require 'matrix'
def MM (a,b)
	return c = a*b
end
a=Matrix.build(3){rand(0..100.00)}
puts "Matrix A is: #{a}\n\n"
b=Matrix.build(3){rand(0..100.00)}
puts "Matrix B is: #{b}\n\n"
puts "Multiplication of Matrix A and Matrix B is (Matrix C):"
puts MM(a,b)

# Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
#    equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
#    by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
#    each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
#    and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function.
#-------------------------------Function 8-------------------------------------
puts "\n\nFunction 8\n----------"
my_file = "index.txt"
def hamming_distance(my_file)
	s = File.readlines(my_file).each_with_index do |read|
	end

	dist = (s[0].chars.zip(s[1].chars)).count {|l, r| l != r}

	open(my_file, 'a') { |f|
	f.puts "\nValue1:Value2:#{dist}"
}
	File.readlines(my_file) do |line|
	puts "#{line}"
	end
end
puts hamming_distance(my_file)

# Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
#    store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
#-------------------------------Function 9-------------------------------------
puts "\n\n"
puts "Function 9"
puts "----------"
class Reader
	require 'csv'
	attr_accessor :csvFile

	def initialize(csvFile)
	@csvFile = csvFile
	end 

	def reading(csvFile)
	CSV.foreach(csvFile) do |row| 
	p row
	end
	end

	def writing(csvFile)
	CSV.open(csvFile, "a+") do |csv|
	csv << ["Class", "cvsReader", "functions", "successfully"]
	end
	end
end

csvFile = "index.csv"
csvObj = Reader.new(csvFile)
puts "Read and display data in a csv File:"
csvObj.reading(csvFile)
csvObj.writing(csvFile)
puts "\nRead and display data in a csv File (after adding more data):"
csvObj.reading(csvFile)

# Function #10: Implement the equivalent of the Class diagram shown in the image link location below:
#    https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
#    This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.
#-------------------------------Function 10-------------------------------------
puts "\n\n"
puts "Function 10"
puts "----------"
class Shape
	attr_accessor :color, :filled

	def initialize(color, filled)
	@color = color
	@filled = filled

	def setColor(color)
	@color = "red"
	end 

	def getColor()
	return color
	end 

	def setFilled(filled)
	@filled = true
	end 

	def isFilled()
	return filled
	end 
end 

class Circle < Shape
	attr_accessor :radius, :area

	def initialize(radius,color,filled)
	@radius = radius
	end 

	def getRadius ()
	return radius
	end 

	def setRadius (radius)
	@radius = radius
	end

	def getArea()
	return 3.14159265359 * radius * radius
	end 

	def getPerimeter()
	return 3.14159265359 * 2 * radius
	end 
end 

class Rectangle < Shape	
	attr_accessor :width, :length, :area

	def initialize(width, length,color,filled)
	@width = width
	@length = length
	end
	
	def setLength (length)
	@length = length
	end
	
	def getLength ()
	return length
	end 

	def setWidth (width)
	@width = width
	end 

	def getWidth ()
	return width
	end 

	def getArea()
	return length * width
	end 

	def getPerimeter()
	return 2 * (length + width)
	end 
end 

class Square < Rectangle
	attr_accessor :side, :area

	def initialize (side,color,filled)
	@side = side
	end 

	def setSide (side)
	@side = side
	end 

	def getSide ()
	return side
	end 

	def getArea()
	return side * side
	end 

	def getPerimeter()
	return 4 * side
	end 
end 

ShapeObj = Shape.new("red",true)
color = ShapeObj.getColor()
filled = ShapeObj.isFilled()

areaCircle = Circle.new(1.0,color,filled)
puts "The radius of a Circle is #{areaCircle.getRadius()}"
puts "=> The area of this Circle is #{areaCircle.getArea()}"
puts "=> The perimeter of this Circle is #{areaCircle.getPerimeter()}"
puts areaCircle.getColor()
areaRectangle = Rectangle.new(1.0, 1.0,color,filled)
puts "\nThe length of a Rectangle is #{areaRectangle.getLength()}, and the width of a Rectangle is #{areaRectangle.getWidth()}"
puts "=> The area of this Rectangle is #{areaRectangle.getArea()}"
puts "=> The perimeter of this Rectangle is #{areaRectangle.getPerimeter()}"

areaSquare = Square.new(2.0,color,filled)
puts "\nThe side of a Square is #{areaSquare.getSide()}"
puts "=> The area of this Square is #{areaSquare.getArea()}"
puts "=> The perimeter of this Square is #{areaSquare.getPerimeter()}"
end