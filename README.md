1. Function#1: Write a function that prints the string “Hello,world.”
2. Function#2: Write a function that accepts 2 parameters: A string and a substring to ﬁnd in that string. 
    The function should ﬁnd the index of substring in string and print that index.
3. Function#3: Write a function that accepts a single parameter – A list/array – and performs 3 actions: (1) Sort the array and print 
    the values, (2) Sort the array in reverse order and print the values, and (3) Prints the value of each array/list item that has a 
    ODD index. The array passed into this function must contain at least 100 randomly generated values. 
    Where appropriate, you should apply as many functional programming techniques(map, filter,etc.) asyou ﬁnd appropriate.
4. Function#4: Generate a list/array of 100 random values. Sort the list using a SelectionSort
5. Function#5: Implement a function to calculate the Fibonacci Numbers iteratively. Attemptto calculate as many Fibonacci numbers as possible. 
    The Fibonacci Series is deﬁned below with F1=F2=1 and F0=0. Fn = Fn−1+Fn−2 
6. Function#6: Implement a function to calculate the Fibonacci series recursively using Guards and PatternMatching. Compare the
    computation time and lines of code with Function#5.
7. Function#7: Implement a function to perform matrix multiplication. The function should take two parameters – Matrix A and Matrix B – and
    return the resultant matrix – Matrix C. Test by generating a variety of random matrices that are ﬁlled with random decimal values between 
    0 and 100. These ﬁles may be in separate ﬁles, classes, etc. and should follow the paradigms of the current language. 
8. Function#8: The Hamming distance between two numbers is the number of bits that must be changed to make the representations of the two numbers 
    equal. For instance, the word “Rob” and “Bob” differ by the ﬁrst letter, so their Hamming distance is 1. Strings like “Rob” and “Robby” differ
    by 2 characters, so the Hamming distance is 2. Write a function that accepts one parameter – a ﬁlename. This ﬁle will contain two strings on 
    each line, with each string being separated by a “ ”. Your function should read each line, calculate the Hamming distance between the two values, 
    and then write the line back to a new ﬁle in the format Value1:Value2:HammingDistance. It is up to you to generate a test ﬁle for this function. 
    The output ﬁle should insert the string _Output into the original ﬁlename. If the input ﬁle is “HammingData.csv” then the output ﬁle would be HammingData_Output.csv.
9. Function #9: Create a csvReader class that is capable of opening, reading, and accessing the data from any given CSV ﬁle. The csvReader class should use a csVRow object to
    store the data from each row in the CSV ﬁle. This function should contain code that demonstrates the ability of your class to read, iterate, and display the contents of a CSV ﬁle.
10. Function #10: Implement the equivalent of the Class diagram shown in the image link location below:
    https://www.ntu.edu.sg/home/ehchua/programming/java/images/ExerciseOOP_ShapeAndSubclasses.png
    This function should contain test code that demonstrates that you have fully implemented these structures and that the inheritance demonstrated works ﬂawlessly.

-----------------------------------------------------
Instructions for compiling and running this Ruby file
-----------------------------------------------------

After downloading all the files in this project and storing them in your directory, there are 2 ways to compile and run:
1. Using git bash:
. Log in to the directory by command: cd PHUONG_HAI_NGUYEN_RUBY
. Compile and run by command: ruby index.rb
-> the solutions for all functions will show up as the example below

2. Using Command Prompt for Windows system
. Log in to the directory by command that contains files: cd /d YOURDIRECTORY (Ex: cd \d C:\Windows)
. Compile and run by command: ruby index.rb
-> the solutions for all functions will show up as the example below


-----------------------------------------------------
        Example Solutions for all functions
-----------------------------------------------------
Function 1
----------
Hello, world.


Function 2
----------
Index of 'World' in 'hello World' is 6


Function 3
----------
Sorted Order
[1, 2, 3, 4, 4, 4, 4, 4, 5, 8, 9, 10, 11, 11, 13, 14, 15, 18, 21, 21, 23, 23, 28, 28, 28, 30, 30, 31, 31, 32, 33, 33, 35, 36, 38, 39, 39, 40, 42, 42, 43, 43, 43, 43, 44, 47, 48, 50, 53, 56, 56, 56, 56, 58, 58, 58, 58, 59, 60, 60, 62, 65, 65, 66, 67, 67, 68, 68, 71, 72, 73, 75, 77, 79, 80, 81, 81, 82, 82, 82, 82, 84, 84, 84, 85, 86, 88, 88, 89, 89, 90, 90, 92, 96, 97, 98, 98, 98, 99, 100]


Reverse Order
[100, 99, 98, 98, 98, 97, 96, 92, 90, 90, 89, 89, 88, 88, 86, 85, 84, 84, 84, 82, 82, 82, 82, 81, 81, 80, 79, 77, 75, 73, 72, 71, 68, 68, 67, 67, 66, 65, 65, 62, 60, 60, 59, 58, 58, 58, 58, 56, 56, 56, 56, 53, 50, 48, 47, 44, 43, 43, 43, 43, 42, 42, 40, 39, 39, 38, 36, 35, 33, 33, 32, 31, 31, 30, 30, 28, 28, 28, 23, 23, 21, 21, 18, 15, 14, 13, 11, 11, 10, 9, 8, 5, 4, 4, 4, 4, 4, 3, 2, 1]


Items with ODD index
[99, 98, 97, 92, 90, 89, 88, 85, 84, 82, 82, 81, 80, 77, 73, 71, 68, 67, 65, 62, 60, 58, 58, 56, 56, 53, 48, 44, 43, 43, 42, 39, 38, 35, 33, 31, 30, 28, 28, 23, 21, 15, 13, 11, 9, 5, 4, 4, 3, 1]


Function 4
----------
Selection Sort:
[0, 0, 1, 3, 4, 5, 5, 6, 10, 11, 12, 12, 13, 14, 15, 16, 17, 18, 18, 18, 18, 20, 22, 23, 23, 23, 24, 24, 24, 27, 27, 29, 29, 31, 33, 34, 35, 36, 38, 38, 39, 40, 41, 41, 43, 47, 47, 48, 49, 49, 53, 54, 55, 55, 55, 56, 57, 58, 58, 59, 60, 62, 62, 65, 65, 66, 66, 67, 68, 69, 70, 70, 73, 76, 78, 78, 79, 79, 80, 81, 82, 82, 83, 84, 85, 87, 88, 90, 92, 92, 93, 94, 95, 96, 98, 98, 98, 98, 99, 99]


Function 5
----------
Fibonacci series of 30 is: 832040


Function 6
----------
Fibonacci series of 10 is: 55


Function 7
----------
Matrix A is: Matrix[[30.03938906036341, 82.6720366303184, 26.953598376157707], [78.83422070207834, 99.5090740307353, 19.45100116884425], [63.99551641125436, 61.93738523335119, 9.940165909816645]]

Matrix B is: Matrix[[20.143374129755966, 25.345262610535414, 9.070851099007948], [7.4440709578550095, 5.588995566425803, 13.250635849165116], [40.03002872743661, 83.87737358051012, 87.46491463350448]]

Multiplication of Matrix A and Matrix B is (Matrix C):
Matrix[[2299.4644766840593, 3484.2068909252075, 3725.434058605312], [3107.363945431196, 4185.728691522088, 3734.932137932927], [2148.057047245348, 2801.9059502752198, 2270.6193002921855]]


Function 8
----------
CS3060, Ruby
CS3140, HTML
Value1:Value2:8


Function 9
----------
Read and display data in a csv File:
["Item", "Cost", "Sold", "Profit"]
["Keyboad", "$10.00 ", "$16.00 ", "$6.00 "]
["Monitor", "$80.00 ", "$120.00 ", "$40 "]
["Mouse", "$5.00 ", "$7.00 ", "$2.00 "]

Read and display data in a csv File (after adding more data):
["Item", "Cost", "Sold", "Profit"]
["Keyboad", "$10.00 ", "$16.00 ", "$6.00 "]
["Monitor", "$80.00 ", "$120.00 ", "$40 "]
["Mouse", "$5.00 ", "$7.00 ", "$2.00 "]
["Class", "cvsReader", "functions", "successfully"]


Function 10
----------
The radius of a Circle is 1.0
=> The area of this Circle is 3.14159265359
=> The perimeter of this Circle is 6.28318530718


The length of a Rectangle is 1.0, and the width of a Rectangle is 1.0
=> The area of this Rectangle is 1.0
=> The perimeter of this Rectangle is 4.0

The side of a Square is 2.0
=> The area of this Square is 4.0
=> The perimeter of this Square is 8.0
